from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from weather.views import save_weather_forecast

urlpatterns = [
    path('', include('weather.urls')),
    path('admin/', admin.site.urls),
    path('save_weather_forecast/',save_weather_forecast, name='save_weather_forecast')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
