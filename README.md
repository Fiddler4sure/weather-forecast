-- WEATHER FORECAST:

- Django application that :
1) Enter any address or city to check the current weather forecast in that location. 
1) use https://www.mapbox.com (also free) to convert an address into a latitude and longitude value.
2) use https://openweathermap.org/ current forecast API to get the weather forecast.
3) use a second button(save) to store weather data into database
4) Use the third button to list all saved weather data


-- TECHNOLOGIES :
- Python3.8
- Django 2.2.28
- Gunicorn(for deployment)
- mysql 8.0
- Docker(docker-compose
- Linux/Ubuntu server

-- DEPLOYMENT :
2) Docker and gunicorn (Quickest approach)

- There are Docker files in the root directory of the project, together with commands.txt(the commands to run, one after the other as they appear) , requirements.txt(required libaries) and .env(for configurations including API keys) files. However you need to have docker-compose installed in your server. Please only make changes on the .env file. Replace DJANGO_HOST = 'your_ip_address'.
- After changing the DJANGO_HOST. Follow the instructions in the commands.txt.
- Please note we are using mysql server within docker, so after command $ -mysql -u root -p , a root password is empty, so just press Enter without typying anything. Then continue with those commands to set up superuser.

- After following all commands. then open your browser and type http://your_ip_address:8000/ and http://your_ip_address:8000/admin for your admin services.