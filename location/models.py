from django.db import models
from config import Config
import geocoder

class City(models.Model):
    name = models.TextField(max_length=150, blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)

    def save(self, *args, **kwargs):
        g = geocoder.mapbox(self.name, key=Config.MAPBOX_ACCESS_TOKEN)
        g = g.latlng # [lat, lng]
        self.lat = g[0]
        self.lon = g[1]
        return super(City, self).save(*args, **kwargs)

    def __str__(self): #show the actual city name on the dashboard
        return self.name

    class Meta: #show the plural of city as cities instead of citys
        verbose_name_plural = 'cities'