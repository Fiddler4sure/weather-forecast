from django.db import models
from location.models import City

# Create your models here.
class Weather(models.Model):
	city = models.OneToOneField(City, on_delete=models.CASCADE,primary_key=True)
	temperature = models.CharField(max_length=10, blank=False)
	description = models.TextField(max_length=150, blank=False, null=False)
	icon = models.CharField(max_length=20, blank=False)

	def __str__(self):
		return f"{self.city.name} is {self.description} at {self.temperature}° F"

	class Meta:
		verbose_name_plural = 'cities'

