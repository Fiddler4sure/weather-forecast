from django.urls import path
from . import views

urlpatterns = [
    # path('', views.index),
    path('', views.home, name='home'),  #the path for our index view
    path('new_search', views.new_search, name='new_search')
]