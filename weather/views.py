from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
import requests
from location.models import City
from location.forms import CityForm
from .models import Weather
from config import Config
import geocoder
from django.contrib import messages


def home(request):
    form = CityForm(request.POST)
    context = {'form' : form, 'selected': False}
    return render(request, 'weather/base.html', context)



def new_search(request):
    form = CityForm(request.POST)
    weather_data = []

    if 'find' in request.POST:

        location = request.POST.get('name',None)
        if not location:
            context = {'form' : form, 'selected': False}
            messages.warning(request, f'Please type an address or city.')
            return render(request, 'weather/base.html', context)

        g = geocoder.mapbox(location, key=Config.MAPBOX_ACCESS_TOKEN)
        g = g.latlng # [lat, lng]
        lat = g[0]
        lon = g[1]

        url = 'https://api.openweathermap.org/data/2.5/weather?lat={}&lon={}&appid={}'
        location_weather = requests.get(url.format(lat , lon , Config.OPENWEATHER_API_KEY)).json()
        weather = {
            'city' : location,
            'temperature' : location_weather['main']['temp'] ,
            'description' : location_weather['weather'][0]['description'],
            'icon' : location_weather['weather'][0]['icon']
        }
        weather_data.append(weather)
        request.session['location'] = location
        context = {'weather_data' : weather_data , 'form' : form , 'selected': True if len(weather_data)== 1 else False}
        return render(request, 'weather/new_search.html', context)


    else:
        
        cities_weather = Weather.objects.all() #return all the weather data in the database
        for city_weather in cities_weather:
            weather = {
                'city' : city_weather.city,
                'temperature' : city_weather.temperature ,
                'description' : city_weather.description,
                'icon' : city_weather.icon
            }

            weather_data.append(weather) #add the data for the current city into our list

        context = {'weather_data' : weather_data ,'form' : form , 'selected': False}
        return render(request, 'weather/new_search.html', context)


def save_weather_forecast(request):
    if request.is_ajax() and request.method == "POST":
        location = request.POST.get('location',None)
        if not location:
            response = { 'status': 404, 'description': 'empty location given' }
            return HttpResponse(response)
        ##Save Location
        if not City.objects.filter(name=location).exists():
            city = City(name=location)
            city.save()

        ## add weather
        city = City.objects.get(name=location)
        if Weather.objects.filter(city=city).exists():
            response = { 'status': 200, 'message': f'{location} weather data has already been successfully saved!' }
            return HttpResponse(response)

        temperature = request.POST.get('temperature')
        description = request.POST.get('description')
        icon = request.POST.get('icon')
        
        weather = Weather.objects.create(city = city, temperature = temperature, description = description, icon = icon)
        weather.save()

        response = { 'status': 200, 'message': f'{location} weather data has been successfully saved!' }
        return HttpResponse(response)
    response = { 'status': 500, 'description': 'Unrecognized request' }
    return HttpResponse(response)